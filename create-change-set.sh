# Turning off the AWS pager so that the CLI doesn't open an editor for each command result
export AWS_PAGER=""

IMAGE_URL=docker.io/sabaork/fastapi-simple-1:latest
IMAGE_PORT=8081

aws cloudformation create-change-set \
  --change-set-name update-ecs-service \
  --stack-name ecs-service \
  --use-previous-template \
  --parameters \
      ParameterKey=StackName,ParameterValue=ecs-network \
      ParameterKey=ServiceName,ParameterValue=ecs-service \
      ParameterKey=ImageUrl,ParameterValue=$IMAGE_URL \
      ParameterKey=ContainerPort,ParameterValue=$IMAGE_PORT \
      ParameterKey=HealthCheckPath,ParameterValue=/ \
      ParameterKey=HealthCheckIntervalSeconds,ParameterValue=50

aws cloudformation describe-change-set \
  --stack-name ecs-service \
  --change-set-name update-ecs-service
