# Turning off the AWS pager so that the CLI doesn't open an editor for each command result
export AWS_PAGER=""

IMAGE_URL=docker.io/sabaork/fastapi-simple-1:latest
IMAGE_PORT=8081

#NETWORK STACK CREATION

aws cloudformation create-stack \
  --stack-name ecs-network \
  --template-body file://network.yml \
  --capabilities CAPABILITY_IAM

aws cloudformation wait stack-create-complete --stack-name ecs-network

#DATABASE STACK CREATION

aws cloudformation create-stack \
  --stack-name rds-database \
  --template-body file://database.yml \
  --parameters \
      ParameterKey=DBName,ParameterValue=sabaork \
      ParameterKey=NetworkStackName,ParameterValue=ecs-network \
      ParameterKey=DBUsername,ParameterValue=sabaork

aws cloudformation wait stack-create-complete --stack-name rds-database

#ECS STACK CREATION

aws cloudformation create-stack \
  --stack-name ecs-service \
  --template-body file://service.yml \
  --parameters \
      ParameterKey=NetworkStackName,ParameterValue=ecs-network \
      ParameterKey=ServiceName,ParameterValue=ecs-service \
      ParameterKey=ImageUrl,ParameterValue=$IMAGE_URL \
      ParameterKey=ContainerPort,ParameterValue=$IMAGE_PORT \
      ParameterKey=HealthCheckPath,ParameterValue=/ \
      ParameterKey=HealthCheckIntervalSeconds,ParameterValue=50 \
      ParameterKey=DatabaseStackName,ParameterValue=rds-database

aws cloudformation wait stack-create-complete --stack-name ecs-service
