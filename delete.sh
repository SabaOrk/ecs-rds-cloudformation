# Turning off the AWS pager so that the CLI doesn't open an editor for each command result
export AWS_PAGER=""

aws cloudformation delete-stack --stack-name ecs-service
aws cloudformation wait stack-delete-complete --stack-name ecs-service

aws cloudformation delete-stack --stack-name rds-database
aws cloudformation wait stack-delete-complete --stack-name rds-database

aws cloudformation delete-stack --stack-name ecs-network
aws cloudformation wait stack-delete-complete --stack-name ecs-network